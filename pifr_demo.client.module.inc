<?php

/**
 * @file
 * Provide Drupal demo site for any pifr test.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Disable pifr testing client reporting.
 */
define('PIFR_CLIENT_REPORT', FALSE);

/**
 * Demo site lifetime in seconds.
 */
define('PIFR_DEMO_LIFETIME', 60 * 60);

/**
 * Demo database prefix.
 */
define('PIFR_DEMO_PREFIX', 'pifr');

/*
/**
 * Implementation of hook_menu().
 */
function pifr_demo_menu() {
  $items = array();

  $items['pifr/demo/start/%'] = array(
    'title' => 'Demo',
    'page callback' => 'pifr_demo_start',
    'page arguments' => array(3),
    'access arguments' => array('access content'),
  );

  return $items;
}

/**
 * Implementation of hook_cron().
 *
 * Remove sites older then the PIFR_DEMO_LIFETIME.
 */
function pifr_demo_cron() {
  // Load database handling.
  module_load_include('review.inc', 'pifr_client');
  $base = drupal_get_path('module', 'pifr_client') . '/review/db';
  $type = pifr_client_review_database_type();
  require_once "$base/db.inc";
  require_once "$base/$type.inc";

  $class = 'pifr_client_db_interface_' . $type;
  $database = new $class;

  // Determine the threshold at which to remove demo sites.
  $threshold = time() - PIFR_DEMO_LIFETIME;
  $files = scandir(file_directory_path() . '/demo');
  foreach ($files as $file) {
    if (is_numeric($file) && $file < $threshold) {
      // Remove checkout files.
      $path = escapeshellarg(file_directory_path() . '/demo/' . $file);
      exec('chmod u+rw -R ' . $path);
      exec('rm -rf ' . $path);

      // Switch pifr_checkout to demo uid and drop database.
      pifr_demo_db_rewrite($file);
      $database->drop_database();
    }
  }
  
  // Remove any orphaned databases.
  $databases = $database->query("SHOW DATABASES LIKE '" . PIFR_DEMO_PREFIX . "%'");
  foreach ($databases as $name) {
    $timestamp = substr($name, 4);
    if ($timestamp < $threshold) {
      pifr_demo_db_rewrite($name);
      $database->drop_database();
    }
  }
}

/**
 * Rewrite the pifr_checkout database URL to use a different database.
 *
 * @param integer $unique_id Unique ID to use for database name.
 */
function pifr_demo_db_rewrite($unique_id) {
  global $db_url;
  $path = parse_url($db_url['pifr_checkout'], PHP_URL_PATH);
  $db_url['pifr_checkout'] = str_replace($path, '/' . PIFR_DEMO_PREFIX . $unique_id, $db_url['pifr_checkout']);
}

/**
 * Start creation of demo site.
 *
 * @param integer $test_id Test ID.
 */
function pifr_demo_start($test_id) {
  $batch = array(
    'operations' => array(
      array('pifr_demo_lock', array()),
      array('pifr_demo_request', array($test_id)),
      array('pifr_demo_generate', array()),
    ),
    'finished' => 'pifr_demo_finished',
    'title' => t('Generating demo site'),
    'init_message' => t('Queueing demo environment generation ...'),
    'progress_message' => t('Processed @current of @total.'),
  );
  batch_set($batch);
  batch_process('node');
}

/**
 * Obtain lock on build environment.
 */
function pifr_demo_lock(&$context) {
  $context['finished'] = 0;
  $context['message'] = t('Waiting for build environment to free up...');

  if (!variable_get('pifr_client_test', FALSE)) {
    // Attempt to lock client for environment generation.
    variable_set('pifr_client_test', $lock = mt_rand(100000, 1000000000));

    // Ensure cache is not used.
    variable_init();

    if (variable_get('pifr_client_test', FALSE) == $lock) {
      $context['finished'] = 1;
    }
  }
}

/**
 * Request test information.
 *
 * @param integer $test_id Test ID.
 */
function pifr_demo_request($test_id, &$context) {
  $url = variable_get('pifr_client_server', '') . '/xmlrpc.php';
  $key = variable_get('pifr_client_key', '');
  $response = xmlrpc($url, 'pifr.demo', $key, (int) $test_id);

  if ($response === FALSE || !empty($response['response'])) {
    $context['results']['error'] = t('Failed to retrieve test information.');
  }
  else {
    $response['review']['plugin'] = 'pifr_demo';
    variable_set('pifr_client_test', $response);
  }

  $context['message'] = t('Test information retrieved.');
}

/**
 * Generate demo environment.
 */
function pifr_demo_generate(&$context) {
  $context['finished'] = 0;

  if ($test = variable_get('pifr_client_test', FALSE)) {
    // If the process has not yet been started then start it, otherwise sleep
    // and report on the status of the environment generation.
    if (!variable_get('pifr_client_test_start', FALSE)) {
      module_load_include('cron.inc', 'pifr_client');
      pifr_client_cron_exec_background('curl --insecure ' . url('pifr/client/review', array('absolute' => TRUE)));

      // Ensure curl call has time to be executed before next batch API loop.
      sleep(2);

      $context['message'] = t('Initializing environment generation.');
    }
    else {
      // Sleep to ensure the batch process to does not loop constantly.
      sleep(3);

      // Get the status message for display.
      module_load_include('review.inc', 'pifr_client');
      $class = pifr_client_review_load($test, TRUE);
      $context['message'] = call_user_func(array($class, 'status'));

      // Abbreviated method for determining a ratio of completion.
      $operations = array('init', 'setup', 'fetch', 'checkout', 'check', 'apply', 'syntax', 'install', 'review');
      $context['finished'] = (array_search(variable_get('pifr_client_operation', 'init'), $operations) + 1) / count($operations);
    }
  }
  else {
    $context['finished'] = 1;
  }
}

/**
 * Upon success redirect user to demo site, otherwise display error.
 */
function pifr_demo_finished($success, $results, $operations) {
  if ($success && !isset($results['error'])) {
    // Redirect to demo site where auto-login should occur.
    drupal_goto('demo/' . variable_get('pifr_demo_id', 'fail'));
  }
  else {
    // Ensure that client is reset after failure.
    variable_del('pifr_client_test');
    drupal_set_message($results['error'], 'error');
  }
}
