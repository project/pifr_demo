<?php

/**
 * @file
 * Provide demo site client review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('client.inc', 'pifr_drupal');

/**
 * Provide demo site client review implementation.
 */
class pifr_client_review_pifr_demo extends pifr_client_review_pifr_drupal {

  /**
   * Unique ID to represent demo site.
   *
   * @var integer
   */
  protected $unique_id;

  /**
   * Create unique checkout directory.
   */
  public function __construct(array $test) {
    parent::__construct($test);

    // Generate unique ID to be used for database name and directory.
    $this->unique_id = time();
    variable_set('pifr_demo_id', $this->unique_id);

    // Set pifr_checkout database to unique id.
    pifr_demo_db_rewrite($this->unique_id);
  }

  /**
   * Move checkout to demo fresh directory.
   */
  protected function review() {
    // Ensure demo directory exists.
    if (!file_exists(file_directory_path() . '/demo')) {
      mkdir(file_directory_path() . '/demo');
    }
    $demo_directory = file_directory_path() . '/demo/' . $this->unique_id;
    mkdir($demo_directory);

    // Generate README.txt containing build information.
    $test_link = variable_get('pifr_client_server', '') . 'pifr/test/' . $this->test['test_id'];
    $file = !empty($this->test['files'][0]) ? $this->test['files'][0] : 'none';
    file_put_contents($this->checkout_directory . '/BUILD.txt',
"-- Generated on " . date('Y-m-d H:i:s') . " --

BUILD INFO
----------
Link: {$this->test['review']['argument']['pifr_demo.link']}
Test: $test_link
File: $file

Enjoy!
");

    // Generate tarball and zip of checkout.
    chdir($this->checkout_directory . '/..');
    $this->exec('tar -cvzf demo/' . $this->unique_id . '/demo.tar.gz checkout');
    $this->exec('zip -r demo/' . $this->unique_id . '/demo.zip checkout');
    chdir($this->original['directory']);

    // Add an auto-login sniplet to index.php.
    $placeholder = $this->test['review']['argument']['core'] == 6 ?
      '$return = menu_execute_active_handler();' : 'menu_execute_active_handler();';

    $contents = file_get_contents($this->checkout_directory . '/index.php');
    $contents = str_replace('menu_execute_active_handler();',
"if (!variable_get('pifr_demo', FALSE)) {
  variable_set('pifr_demo', TRUE);
  drupal_set_message('Copy of code avaiable as <a href=\"demo.tar.gz\">tar.gz</a> or <a href=\"demo.zip\">zip</a>.');
  drupal_goto(user_pass_reset_url(user_load(1)));
}
" . $placeholder, $contents);
    file_put_contents($this->checkout_directory . '/index.php', $contents);

    // Move checkout to demo directory.
    $this->exec('mv ' . $this->checkout_directory . '/* ' . $demo_directory);
  }
}
