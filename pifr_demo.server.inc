<?php

/**
 * @file
 * Provide demo site server review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('server.inc', 'pifr_drupal');

/**
 * Provide demo site server review implementation.
 */
class pifr_server_review_pifr_demo extends pifr_server_review_pifr_drupal {

}
