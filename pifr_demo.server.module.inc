<?php

/**
 * @file
 * Provide Drupal demo site for any pifr test.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_menu().
 */
function pifr_demo_menu() {
  $items = array();

  $items['pifr/demo/%'] = array(
    'title' => 'Demo',
    'page callback' => 'pifr_demo_trigger',
    'page arguments' => array(2),
    'access arguments' => array('trigger pifr_demo'),
  );
  $items['admin/pifr/demo'] = array(
    'title' => 'Demo',
    'description' => 'Configure demo settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_demo_settings_form'),
    'access arguments' => array('administer pifr'),
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function pifr_demo_perm() {
  return array(
    'trigger pifr_demo',
    'administer pifr_demo',
  );
}

/**
 * Implementation of hook_xmlrpc().
 */
function pifr_demo_xmlrpc() {
  return array(
    array('pifr.demo', 'pifr_demo_retrieve', array('struct', 'string', 'int'),
           t('Request the next test for generating demo site. (Called by PIFR demo client)')),
  );
}

/**
 * Trigger creation of demo site.
 *
 * Confirm test ID is valid and redirect to demo client.
 *
 * @param integer $test_id Test ID.
 */
function pifr_demo_trigger($test_id) {
  if ($test = pifr_server_test_get($test_id)) {
    if ($url = variable_get('pifr_demo_server_url', FALSE)) {
      drupal_goto($url . 'pifr/demo/start/' . $test_id);
    }
    else {
      drupal_set_message(t('This server has not yet been configured.'), 'error');
    }
  }
  else {
    drupal_set_message(t('Invalid test ID.'), 'error');
  }

  return '';
}

/**
 * Provide the XML-RPC equivilent of pifr.next() for a specific test ID.
 *
 * @param string $key Server key.
 * @param integer $test_id Test ID.
 * @return array Test information, or XML-RPC response code.
 */
function pifr_demo_retrieve($key, $test_id) {
  if ($key == variable_get('pifr_demo_server_key', FALSE)) {
    if (PIFR_ACTIVE) {
      if ($test = pifr_server_test_get($test_id)) {
        if ($environments = pifr_server_environment_test_get_all($test_id)) {
          // Get the object related to the test and pass along its link value.
          if ($test['type'] == PIFR_SERVER_TEST_TYPE_BRANCH) {
            $object = pifr_server_branch_get_test($test_id);
          }
          elseif ($test['type'] == PIFR_SERVER_TEST_TYPE_FILE) {
            $object = pifr_server_file_get_test($test_id);
          }
          else {
            return array('response' => PIFR_RESPONSE_DENIED);
          }

          // Add link to review arguments.
          $xmlrpc = pifr_server_test_xmlrpc($test, array_shift($environments));
          $xmlrpc['review']['argument']['pifr_demo.link'] = $object['link'];

          return $xmlrpc;
        }
      }
    }
    return array('response' => PIFR_RESPONSE_DENIED);
  }
  return array('response' => PIFR_RESPONSE_INVALID_SERVER);
}

/**
 * Demo settings form.
 */
function pifr_demo_settings_form(&$form_state) {
  $form = array();

  if (!variable_get('pifr_demo_server_key', FALSE)) {
    variable_set('pifr_demo_server_key', md5(uniqid()));
  }

  $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server'),
    '#description' => t('The URL of the demo server and key it will use to authenticate itself.'),
  );
  $form['server']['pifr_demo_server_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('pifr_demo_server_key', ''),
  );
  $form['server']['pifr_demo_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#required' => TRUE,
    '#default_value' => variable_get('pifr_demo_server_url', ''),
  );

  return system_settings_form($form);
}
